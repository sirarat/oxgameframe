/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sirarat.oxgameui;

import java.io.Serializable;

/**
 *
 * @author admin02
 */
public class Table implements Serializable {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastrow;
    private int lastcol;
    private Player winner;
    private boolean finish = false;
    private int count;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println(" 123");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");

        }
    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            checkWin();
            return true;

        }
        return false;

    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
        count++;

    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();

        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkdiagonally() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[j][j] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();

    }

    void checkdiagonally1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0, k = 2; j < 3; j++, k--) {
                if (table[j][k] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();

    }

    void checkDraw() {
        if (count >= 8) {
            finish = true;
            playerO.draw();
            playerX.draw();
        }
    }

    void checkWin() {
        checkRow();
        checkCol();
        checkdiagonally();
        checkdiagonally1();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

}
